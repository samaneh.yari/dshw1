import java.util.Arrays;
import java.util.Scanner;

public class ElectericYaghub {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] numbers = new int[n];
        long x = sc.nextLong();
        for (int i = 0; i < n; i++)
            numbers[i] = sc.nextInt();
        Arrays.sort(numbers);
        int f = 0;
        int r = numbers.length;
        int t = 0;
        while (f != r) {
            t++;
            int i =numbers[--r];
            if (f != r)
                if (i + numbers[f] <= x)
                    f++;
        }
        System.out.println(t);
    }

}
// 5 10 7 2 3 9 1
// 6 10 7 2 3 9 1 1s