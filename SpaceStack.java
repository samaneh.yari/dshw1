import java.util.ArrayList;
import java.util.Scanner;

public class SpaceStack {
    ArrayList<Integer> stack,min;

    public SpaceStack() {
        this.min = new ArrayList<>();
        this.stack = new ArrayList<>();
    }
    public void push(int x)
    {
        if(stack.size()==0){
            stack.add(x);
            min.add(x);
        }else {
            if(x<min.get(min.size()-1))
                min.add(x);
            else
                min.add(min.get(min.size()-1));
            stack.add(x);
        }
    }

    public int pop(){
        int t=stack.remove(stack.size()-1);
        min.remove(min.size()-1);
        return t;
    }

    public int spell(){
        return min.get(min.size()-1);
    }

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        SpaceStack spaceStack=new SpaceStack();
        for (int i = 0; i < n; i++) {
            String cm=sc.next();
            if(cm.equals("push"))
                spaceStack.push(sc.nextInt());
            else if(cm.equals("pop"))
                spaceStack.pop();
            else
                System.out.println(spaceStack.spell());
        }
    }
}
// 9 push 3 push 2 spell push 1 spell pop spell pop spell