import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class StartUpQueue {
    ArrayList<Integer> queue;
    ArrayList<Integer>undo;

    public StartUpQueue() {
        queue = new ArrayList<>();
        undo = new ArrayList<>();
    }

    public void enQueue(int x) {
        queue.add(x);
        undo.add(null);
    }

    public int pop() {
        int ff = queue.remove(0);
        undo.add(ff);
        return ff;
    }

    public void undo() {
        if (undo.get(undo.size()-1)==null)
            queue.remove(queue.size()-1);
        else
            queue.add(0,undo.get(undo.size()-1));
    }

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        StartUpQueue startUpQueue=new StartUpQueue();
        for (int i = 0; i < n; i++) {
            String cm=sc.next();
            if(cm.equals("enqueue"))
                startUpQueue.enQueue(sc.nextInt());
            else if(cm.equals("pop"))
                System.out.println(startUpQueue.pop());
            else
                startUpQueue.undo();
        }
    }

}
// 10 enqueue 1 enqueue 2 pop undo pop enqueue 3 undo pop enqueue 10 pop