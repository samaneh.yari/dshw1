import java.util.Scanner;

public class Parentesis {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        String o=sc.next();
        int j=0;
        int max=1;
        int k=0;
        for(int i=0;i<n;i++){
            if(o.charAt(i)=='(')
                j++;
            else if(o.charAt(i)==')')
                j--;
            if(j==0) {
                if(k>max)
                    max=k;
                k = 0;
            }else k++;
        }
        System.out.println(max);
    }
}
//10 (()(()))()
//4 ()()
//6 (()())